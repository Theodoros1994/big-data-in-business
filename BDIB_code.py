#Language Python

import pandas as pd
import pandas_ml as pdml
from sklearn.grid_search import GridSearchCV # used to find the optimal parameters for every classifier
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report,confusion_matrix #evaluation of classifiers
from sklearn.metrics import f1_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_curve, auc
from sklearn.ensemble import (RandomForestClassifier, 
                               GradientBoostingClassifier)
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
from mlxtend.frequent_patterns import association_rules
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import apriori
import re
from sklearn.externals.six import StringIO 
import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'
from collections import Counter
import seaborn as sns
import statsmodels.formula.api as sm


zip = zipfile.ZipFile(r'/home/acggs/Downloads/bank.zip')  
zip.extractall(r'/home/acggs/Downloads/')  
path=r'/home/acggs/Downloads/bank-full.csv' 
data=pd.read_csv(path,delimiter=';')

#rename the y column
data['y']=data.rename(columns={'y':'outcome'},inplace=True)
data=data.drop(['y','contact','day','month'],1)

count=Counter(data['outcome'])
count.keys()
positive=0
negative=0

for key , value in count.items():
    if key =='yes':
        print 'There are:',value,'people who were positive to the makreting campaign'
        positive=positive+value
    else:
        print 'There are:',value,'people who were negative to the marketing campaign'
        negative=negative+value

data_list=[data]

def mapping(y):
    for dataset in y[:]:
#Mapping age
        max_age=max(data['age'])
        min_age=min(data['age'])
        max_age=max(data2['age'])
        min_age=min(data2['age'])
        age_range=max_age-min_age
        dataset.loc[ dataset['age']<=min_age+(age_range/3),'age']=0
        dataset.loc[(dataset['age']>min_age+(age_range/3)) & (dataset['age'] <=min_age+2*(age_range/3)),'age']=1
        dataset.loc[(dataset['age']>min_age+2*(age_range/3)) ,'age']=2
        
    return y

data_list=mapping(data_list)
data['age'].replace({0:'young',1:'middle age',2:'senior'},inplace=True)

def graph(a,b,c,d,e):
#Visualize 
    pd.crosstab(a,data['outcome']).plot(kind='bar')
    plt.title('Marital Status graph')
    L=plt.legend()
    L.get_texts()[0].set_text('negative')
    L.get_texts()[1].set_text('positive')

    pd.crosstab(b,data['outcome']).plot(kind='bar')
    plt.title('Education Level graph')
    L=plt.legend()
    L.get_texts()[0].set_text('negative')
    L.get_texts()[1].set_text('positive')

    pd.crosstab(c,data['outcome']).plot(kind='bar')
    plt.title('Housing Loans graph')
    L=plt.legend()
    L.get_texts()[0].set_text('negative')
    L.get_texts()[1].set_text('positive')

    pd.crosstab(d,data['outcome']).plot(kind='bar')
    plt.title('Personal Loans graph')
    L=plt.legend()
    L.get_texts()[0].set_text('negative')
    L.get_texts()[1].set_text('positive')
    
    pd.crosstab(e,data['outcome']).plot(kind='bar')
    plt.title('Age graph')
    L=plt.legend()
    L.get_texts()[0].set_text('negative')
    L.get_texts()[1].set_text('positive')
    
import matplotlib as plt
_ = data.hist(column=continuous_vars, figsize = (16,16))

# plot correlation matrix
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = numpy.arange(0,9,1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(correlations.columns)
ax.set_yticklabels(correlations.columns)
plt.show()


sns.pairplot(data)
sns.plt.show()
cor=data.corr()
# Count plots of categorical variables

fig, axes = plt.subplots(4, 3, figsize=(16, 16))
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.7, hspace=0.3)

for i, ax in enumerate(axes.ravel()):
    if i > 9:
        ax.set_visible(False)
        continue
    sns.countplot(y = categorical_vars[i], data=data, ax=ax)


def plot_corr(df,size=10):
    '''Function plots a graphical correlation matrix for each pair of columns in the dataframe.

    Input:
        df: pandas DataFrame
        size: vertical and horizontal size of the plot'''

    corr = df.corr()
    fig, ax = plt.subplots(figsize=(size, size))
    ax.matshow(corr)
    plt.xticks(range(len(corr.columns)), corr.columns);
    plt.yticks(range(len(corr.columns)), corr.columns);



plot_corr(data,size=10)

print graph(data['marital'],data['education'],data['housing'],data['loan'],data['age'])

#map columns
marital_status_mapping={'married':0,'divorced':1,'single':2}
data['marital']=data['marital'].map(marital_status_mapping)

education_mapping={'unknown':0,'primary':1,'secondary':2,'tertiary':3}
data['education']=data['education'].map(education_mapping)

default_mapping={'no':0,'yes':1}
data['default']=data['default'].map(default_mapping)

housing_mapping={'no':0,'yes':1}
data['housing']=data['housing'].map(housing_mapping)

loan_mapping={'no':0,'yes':1}
data['loan']=data['loan'].map(loan_mapping)

outcome_mapping={'no':0,'yes':1}
data['outcome']=data['outcome'].map(outcome_mapping)

poutcome_mapping={'failure':0,'success':1,'unknown':2,'other':3}
data['poutcome']=data['poutcome'].map(poutcome_mapping)

job_mapping={"admin.":0,"unknown":1,"unemployed":2,"management":3,"housemaid":4,"entrepreneur":5,"student":6,
                                       "blue-collar":7,"self-employed":8,"retired":9,"technician":10,"services":11}
data['job']=data['job'].map(job_mapping)

age_mapping={"young":0,"middle age":1,"senior":2}
data['age']=data['age'].map(age_mapping)

for dataset in data_list:
    print("+++++++++++++++++++++++++++")
    print(pd.isnull(dataset).sum() >0)
    print("+++++++++++++++++++++++++++")

if pd.isnull(dataset).sum().all()==0:
    print 'There are no missing values'
else:
    print 'There are missing values'

df = pdml.ModelFrame(data,target=np.array([0, 1]).repeat([positive, negative]))
df.target.value_counts()
sampler = df.imbalance.over_sampling.SMOTE()
sampled = df.fit_sample(sampler)

# Correlation graph between all the variables
df.corr()
plt.figure()
sns.heatmap(df.corr())

#convert list to dataframe
features = df.dtypes.index
features = list(df.columns[0:14])
y = data["outcome"]
X = df[features]
X = preprocessing.scale(X)

# Create train test dataframes
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

# =============================================================================
#                        Define models for GridSearch
# =============================================================================
models = {
          'RandomForestClassifier': RandomForestClassifier(),
          'tree':DecisionTreeClassifier(),
          'GradientBoostingClassifier': GradientBoostingClassifier(),
          'log':LogisticRegression(),
          }

#pass parameteres to test for our models
params = {
        'RandomForestClassifier': {'n_estimators': [10,100,1000],
                                   'max_features': ['auto', 'sqrt', 'log2'],
                                   'criterion' : ['gini', 'entropy'] },
        'tree':{'max_depth': [1, 2, 5],'max_features': [1,4]},
        'GradientBoostingClassifier': { 'n_estimators': [16, 32], 'learning_rate': [0.8, 1.0] },
        'log': {'solver' : ['newton-cg','lbfgs','liblinear'],'C':[0.001, 0.01, 0.1, 1, 10, 100, 1000]},
        }       
# =============================================================================
#                              Helping Functions
# =============================================================================
class EstimatorSelectionHelper:
    def __init__(self, models, params):
        if not set(models.keys()).issubset(set(params.keys())):
            missing_params = list(set(models.keys()) - set(params.keys()))
            raise ValueError("Some estimators are missing parameters: %s" % missing_params)
        self.models = models
        self.params = params
        self.keys = models.keys()
        self.grid_searches = {}

    def fit(self, X, y, cv=3, n_jobs=1, verbose=1, scoring=None, refit=False):
        for key in self.keys:
            print("Running GridSearchCV for %s." % key)
            model = self.models[key]
            params = self.params[key]
            gs = GridSearchCV(model, params, cv=cv, n_jobs=n_jobs, 
                              verbose=verbose, scoring=scoring, refit=refit)
            gs.fit(X,y)
            self.grid_searches[key] = gs    

    def score_summary(self, sort_by='mean_score'):
        def row(key, scores, params):
            d = {'estimator' : key,
                 'min_score' : min(scores),
                 'max_score' : max(scores),
                 'mean_score': np.mean(scores),
                 'std_score' : np.std(scores)
            }
            return pd.Series(dict(params.items()+d.items()))
      
        rows = [row(k, gsc.cv_validation_scores, gsc.parameters) 
                     for k in self.keys
                     for gsc in self.grid_searches[k].grid_scores_]
        df = pd.concat(rows, axis=1).T.sort_values([sort_by], ascending=False)

        columns = ['estimator', 'min_score', 'mean_score', 'max_score', 'std_score']
        columns = columns + [c for c in df.columns if c not in columns]
        return df[columns]

def report_to_df(report):
    report = re.sub(r" +", " ", report).replace("avg / total", "avg/total").replace("\n ", "\n")
    report_df = pd.read_csv(StringIO("Classes" + report), sep=' ', index_col=0)        
    return(report_df)

def matrix_to_df(m):
    a=pd.DataFrame(m)
    return(a)
 
# pass models and their params and fit data
helper = EstimatorSelectionHelper(models, params)
helper.fit(X_train, y_train)

#produce dataframe with all combinations
a=helper.score_summary(sort_by='min_score')

# create a dataframe containing only the best parameteres
best_param=a.sort_values('min_score', ascending=False).drop_duplicates('estimator').sort_index().reset_index()
best_param=best_param.drop(['index'],1)
best_param.set_index('estimator', inplace=True)
best_param=best_param.transpose()
# =============================================================================
#               Assign the best parameters to the models
# =============================================================================

rfc = RandomForestClassifier(n_estimators=best_param['RandomForestClassifier']['n_estimators'],
                            criterion=best_param['RandomForestClassifier']['criterion'])

dtree = DecisionTreeClassifier(max_depth=best_param['tree']['max_depth'],
                               max_features=best_param['tree']['max_features'])

clf_gb = GradientBoostingClassifier(n_estimators=best_param['GradientBoostingClassifier']['n_estimators'],
                                    learning_rate=best_param['GradientBoostingClassifier']['learning_rate'])

logmodel=LogisticRegression(solver=best_param['log']['solver'],
                            C=best_param['log']['C'])

# =============================================================================
#                            Fit Models & Extract Scores
#               define models, predictions, reports and put them in matrices
# =============================================================================

def fit(x,y,z,w):
    rfc.fit(x,y)
    rfc_train_predictions = rfc.predict(x)
    rfc_test_predictions = rfc.predict(z)
    RandomF_report=report_to_df(classification_report(w,rfc_test_predictions))
    
    dtree.fit(x,y)
    dtree_train_predictions = dtree.predict(x)
    dtree_test_predictions = dtree.predict(z)
    decision_trees_classification_report=report_to_df(classification_report(w,dtree_test_predictions))
    
    clf_gb.fit(x,y)
    clf_gb_train_predictions= clf_gb.predict(x)
    clf_gb_test_predictions= clf_gb.predict(z)
    clf_gb_report=report_to_df(classification_report(w,clf_gb_test_predictions))
    
    logmodel.fit(x,y)
    log_train_predictions= logmodel.predict(x)
    log_test_predictions= logmodel.predict(z)
    log_report=report_to_df(classification_report(w,log_test_predictions))

        
    rfc_conf_matrix=matrix_to_df(confusion_matrix(w,rfc_test_predictions))
    dtree_conf_matrix=matrix_to_df(confusion_matrix(w,dtree_test_predictions))
    log_conf_matrix=matrix_to_df(confusion_matrix(w,log_test_predictions))
    clf_gb_conf_matrix=matrix_to_df(confusion_matrix(w,clf_gb_test_predictions))

#Designate an array of matrices
    matrix=[rfc_conf_matrix,dtree_conf_matrix,
            log_conf_matrix,
           clf_gb_conf_matrix]
    
#Designate an array of prediction_methods    
    prediction_methods=['Random Forest','Decision Tree','Log',
                 'CLF-GB','Neural Networks']
    
#Designate an array of train_predictions              
    train_predictions=[rfc_train_predictions,
                       dtree_train_predictions,
                       log_train_predictions,
                       clf_gb_train_predictions]
    
#Designate an array of train_predictions      
    test_predictions=[rfc_test_predictions,
                       dtree_test_predictions,
                       log_test_predictions,
                       clf_gb_test_predictions]
    
    reports=[RandomF_report,decision_trees_classification_report,
             log_report,clf_gb_report]
    
    e=zip(prediction_methods, matrix,train_predictions,test_predictions,reports)
    for i,j,k,l,m in e:
        print '-------------------------',i,'method--------------------------'
        print 'The confusion matrix for',i,'method is\n',j,'\n'
        print '===>>> Macro'
        print 'Train f1 for',i,'method is=',format(f1_score(y, k, average='macro'),'.4f'),'\n'
        print 'Test f1 for',i,'method is=',format(f1_score(w, l, average='macro'),'.4f'),'\n'
        print 'Train precision for',i,'method is=',precision_recall_fscore_support(y, k, average='macro'),'\n'
        print 'Test precision for',i,'method is=',precision_recall_fscore_support(w, l, average='macro'),'\n'
        print '===>>> Micro'
        print 'Train f1 for',i,'method is=',format(f1_score(y, k, average='micro'),'.4f'),'\n'
        print 'Test f1 for',i,'method is=',format(f1_score(w, l, average='micro'),'.4f'),'\n'
        print 'Train precision for',i,'method is=',precision_recall_fscore_support(y, k, average='micro'),'\n'
        print 'Test precision for',i,'method is=',precision_recall_fscore_support(w, l, average='micro'),'\n'
        print '===>>> None'
        print 'Train f1 for',i,'method is=',f1_score(y, k, average=None),'\n'
        print 'Test f1 for',i,'method is=',f1_score(w, l, average=None),'\n'
        print 'Train precision for',i,'method is=',precision_recall_fscore_support(y, k, average=None),'\n'
        print 'Test precision for',i,'method is=',precision_recall_fscore_support(w, l, average=None),'\n'
        print 'Test report for the ',i,'method is:',m,'\n'
        fpr, tpr, threshold = roc_curve(y, k)
        roc_auc = auc(fpr, tpr)
        plt.title('Receiver Operating Characteristic')
        plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
        plt.legend(loc = 'lower right')
        plt.plot([0, 1], [0, 1],'r--')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.show()
    import plotly
    plotly.offline.init_notebook_mode()
    import plotly.graph_objs as go
    import cufflinks as cf
    cf.go_offline()
    from plotly.graph_objs import *
    from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
    x_data= [RandomF_report.values[-1].tolist(),
             decision_trees_classification_report.values[-1].tolist(),
             clf_gb_report.values[-1].tolist(),
             log_report.values[-1].tolist()]
    precision = [item[0] for item in x_data]
    recall = [item[1] for item in x_data]
    f1 = [item[2] for item in x_data]
    support = [item[3] for item in x_data]
    y= ['RANDOM FORESTS ','DECISION TREES ',
        'GB','LOG'],
    trace1 = go.Bar(
            y= ['RANDOM FORESTS ','DECISION TREES ',
        'GB','LOG'],
            x=precision,
            name='precision',
            orientation = 'h',
            marker = dict(color = 'rgba(246, 78, 139, 0.6)',
                          line = dict(color = 'rgba(246, 78, 139, 1.0)',width = 3)))
    trace2 = go.Bar(
    y= ['RANDOM FORESTS ','DECISION TREES ',
        'GB','LOG'],
        x=recall,
        name='recall',
        orientation = 'h',
        marker = dict(color = 'rgba(58, 71, 80, 0.6)',
                      line = dict(color = 'rgba(58, 71, 80, 1.0)',width = 3)))
    trace3 = go.Bar(
    y= ['RANDOM FORESTS ','DECISION TREES ',
        'GB','LOG'],
    x=f1,
    name='f1 score',
    orientation = 'h',
    marker = dict(color = 'rgba(83, 159, 80, 0.6)',
                  line = dict(color = 'rgba(83, 159, 80, 1.0)',width = 3)))
    data = [trace1, trace2,trace3]
    layout = go.Layout(barmode='stack')
    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='marker-h-bar.html')

print fit(X_train,y_train,X_test,y_test)

# =============================================================================
#                                 Association Rules
# =============================================================================
data2=pd.read_csv(path,delimiter=';')

#rename the y column
data2['y']=data2.rename(columns={'y':'outcome'},inplace=True)
data2=data2.drop(['y','contact','day','month'],1)

data_list2=[data2]

data_list2=mapping(data_list2)
data2['age'].replace({0:'young',1:'middle age',2:'senior'},inplace=True)

#Mapping outcome
data2['outcome']=data2['outcome'].map(outcome_mapping)
bins = [0,1,2]
labels = ['positive', 'negative']
data2['outcome']=pd.cut(data2['outcome'], bins=bins, labels=labels,right=False)

df_pos=data2.ix[data2['outcome'] == 'positive']
data_list2=df_pos.values.tolist()

te = TransactionEncoder()
te_ary = te.fit(data_list2).transform(data_list2)
df_ar = pd.DataFrame(te_ary, columns=te.columns_)
apriori(df_ar, min_support=0.6, use_colnames=True)
frequent_itemsets = apriori(df_ar, min_support=0.1, use_colnames=True)

#produced rules with 60% of total combinations, later we will apply confidence to algorithm 
rules=association_rules(frequent_itemsets,metric="lift",min_threshold=0.8)
rules['consequents_len']=rules['consequents'].apply(lambda x:len(x))
rules['antecedants_len']=rules['antecedants'].apply(lambda x:len(x))
positive_percentages=rules[((rules['consequents_len']==1) & (rules['consequents']==frozenset(['positive'])) & (rules['antecedants_len']>1))]
positive_percentages=pd.DataFrame(positive_percentages)
positive_percentages=positive_percentages.sort_values(by='support',ascending=[False])
positive_percentages.to_csv("/home/acggs/Downloads/association_rules.csv")

csv=pd.read_csv(r'/home/acggs/Downloads/association_rules.csv')
csv['antecedants'] = csv['antecedants'].str[10:-1]
csv['consequents'] = csv['consequents'].str[10:-1]
csv.to_csv("/home/acggs/Downloads/association_rules.csv")
















#Language R

install.packages("ggplot2")
library(ggplot2)
install.packages("ggplot")
library(ggplot)
install.packages("caret")
library(caret)
install.packages("caretEnsemble")
library(caretEnsemble)
install.packages("ROSE")
library(ROSE)
install.packages("mlbench")
library(mlbench)
install.packages("DMwR")
library(DMwR)
install.packages("rpart")
library(rpart)
install.packages("rattle")
library(rattle)
install.packages("rpart.plot")
library(rpart.plot)
install.packages("RColorBrewer")
library(RColorBrewer)
library(rpart.plot)
install.packages("corrplot")
library(corrplot)
corrplot(mydata, method="circle", type="upper")
install.packages(c("rpart","rpart.plot","rattle"))

library(rpart)
library(rpart.plot)
library(rattle)


install.packages("corrplot")
library(corrplot)
mydata.rpart <- rpart(y ~ ., data = mydata)
fancyRpartPlot(mydata.rpart)
mydata <- read.csv(file="C:/Users/teo/Pictures/bank.csv", header=TRUE,sep=";")
#Summary on dataset
summary(mydata)
corrplot(mydata, method="circle")
str(mydata)

p_age <- ggplot(mydata, aes(factor(y), age)) + geom_boxplot(aes(fill = factor(y)))
p_age

p_balance <- ggplot(mydata, aes(factor(y), balance)) + geom_boxplot(aes(fill = factor(y)))
p_balance

p_day <- ggplot(mydata, aes(factor(y), day)) + geom_boxplot(aes(fill = factor(y)))
p_day

p_duration <- ggplot(mydata, aes(factor(y), duration)) + geom_boxplot(aes(fill = factor(y)))
p_duration

p_campaign <- ggplot(mydata, aes(factor(y), campaign)) + geom_boxplot(aes(fill = factor(y)))
p_campaign

p_pdays <- ggplot(mydata, aes(factor(y), pdays)) + geom_boxplot(aes(fill = factor(y)))
p_pdays

p_previous <- ggplot(mydata, aes(factor(y), previous)) + geom_boxplot(aes(fill = factor(y)))
p_previous



dataSubsetBalance <- mydata[,c(4,12, 17)]
C <- mydata[,c(4, 49)]
p3 <- ggplot(dataSubsetBalance, aes(education, fill = y)) + geom_density(alpha = 0.3) +
  xlab("Call duration - density") +
  ylab("Bank account balance (€)")
p3


# Final Boxplot !
boxplot(C , col=rgb(0.3,0.2,0.5,0.6) , ylab="Class" )

